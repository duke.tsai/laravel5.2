<?php

namespace App\Repositories;

use DB;

class UserRepository extends BaseRepository
{
    public function getUser($queryData)
    {
        $user = DB::table('adm_user')
					->join('adm_role', 'adm_user.adm_role_id', '=', 'adm_role.id')
					->select('adm_user.account','adm_role.name','adm_role.work_list','adm_user.adm_role_id')
					->where('adm_user.account', $queryData['name'])
					->Where('adm_user.password', md5($queryData['password']))
					->first();

        return $user;
    }
	
	public function updateUserLoginTime($user)
    {
        $user = DB::table('adm_user')
					->where('account', $user->account)
					->update(array('updated_at' => date("Y-m-d H:i:s")));

        return $user;
    }
	
	public function getUserMenu($user)
    {
        $menuList = DB::table('adm_menu')
						->whereIn('id',array_map('intval', explode(',', $user->work_list)))
						->get();

        return $menuList;
    }
}
