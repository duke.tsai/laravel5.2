<?php

namespace App\Repositories;

use DB;

class ProjectRepository extends BaseRepository
{
    public function getProjectAll()
    {
        $projectList = DB::table('project')->paginate($this->pageCnt);

        return $projectList;
    }
}
