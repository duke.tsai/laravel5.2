<?php

namespace App\Services\backend;

use App\Services\BaseService;
use App\Repositories\UserRepository;

class UserService extends BaseService
{	
	public function __construct(UserRepository $userRepository){
		
        $this->userRepository = $userRepository;
    }
	
	public function getUser($queryData){
		
		$user = $this->userRepository->getUser($queryData);
		
		return $user;
    }
	
	public function getUserMenu($user){
		
		$this->userRepository->updateUserLoginTime($user);
		
		if(!empty($user->work_list)){
			
			$menuList = $this->userRepository->getUserMenu($user);
		}
		
		return $menuList;
    }

}
