<?php

namespace App\Services\backend;

use App\Services\BaseService;
use App\Repositories\ProjectRepository;

class ProjectService extends BaseService
{	
	public function __construct(ProjectRepository $projectRepository){
		
        $this->projectRepository = $projectRepository;
    }
	
	public function getProjectAll(){
		
		$projectList = $this->projectRepository->getProjectAll();
		
		return $projectList;
    }

}
