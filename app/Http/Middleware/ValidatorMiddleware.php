<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class ValidatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$folder = $request->route()->getAction()['middleware'][0];
		$vClass = 'App\\Validators\\'.$folder.'\\'.str_replace('Controller', '', explode('@', class_basename($request->route()->getActionName()))[0]).'Validator';
        $method = explode('@', $request->route()->getActionName())[1];
		
		if (method_exists($vClass,$method)) {
			
			$validator = new $vClass($request);
			$validation = $validator->$method();
			
			if ($validation->fails()) {
				
				if ($folder == 'web' || $folder == 'backend') {

					return Redirect::back()->withErrors($validation->errors()->all())->withInput();
					
				}else{
					
					return response()->json(['errors' => $validation->errors()->all()], 400);

				}
			}
		}
        
        return $next($request);
    }
}
