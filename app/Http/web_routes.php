<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => 'clear'. (env('APP_ENV') == 'production' ? '' : '-'.env('APP_ENV')) .'-project.tw',
				'middleware' => 'web', 'namespace' => 'web'], function () {
    
	Route::get('/', 'HomeController@index');
	Route::post('/login', 'LoginController@login');
});
