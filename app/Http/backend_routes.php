<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => 'clear'. (env('APP_ENV') == 'production' ? '' : '-'.env('APP_ENV')) .'-admin-project.tw',
				'middleware' => 'backend', 'namespace' => 'backend'], function () {
    
	Route::get('/', 'LoginController@login');
	Route::post('/home', 'LoginController@home');
	Route::get('/project', 'ProjectController@index');
});
