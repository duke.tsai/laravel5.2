<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PageController extends Controller
{
    public function getPage($dataList){
		
		$pageRange = 4; //頁碼左右頁數
		
		if($dataList->lastPage() < 10 || $dataList->currentPage() <= $pageRange){
			$page = range(1,9);
		}else if($dataList->currentPage() > $dataList->lastPage() - $pageRange){
			$page = range($dataList->lastPage()-2*$pageRange,$dataList->lastPage());
		}else{
			$page = range($dataList->currentPage()-$pageRange,$dataList->currentPage()+$pageRange);
		}

		return $page;
	} 
}
