<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageController;
use App\Services\backend\ProjectService;
use Redirect;
use Session;

class ProjectController extends Controller
{
	public function __construct(PageController $pageController, ProjectService $projectService)
    {							
		parent::__construct();
		
		$this->pageController = $pageController;
		$this->projectService = $projectService;
    }
	
	public function index(Request $request){

		$projectList = $this->projectService->getProjectAll();
		$page = $this->pageController->getPage($projectList);
		
		return view($this->view,['dataList' => $projectList, 'page' => $page]);
	}

}
