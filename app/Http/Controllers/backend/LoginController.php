<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect;
use App\Services\backend\UserService;
use Session;
use Redis;

class LoginController extends Controller
{
	public function __construct(UserService $userService)
    {							
		parent::__construct();
		
		$this->userService = $userService;
    }
	
    public function login(Request $request){
		
		return view($this->view);
	}
	
	public function home(Request $request){
		
		$queryData = $request->only('name','password');
		
		$user = $this->userService->getUser($queryData);
		
		if(empty($user)){
			
			return Redirect::back()->withErrors(['msg'=>'帳號密碼錯誤，請重新輸入'])->withInput();
		}else{
			
			$menuList = $this->userService->getUserMenu($user);
			
			Session::put('user', $user);
			Session::put('menuList', $menuList);
			Session::put('title', '首頁');
			Redis::set('name', 'Taylor');
			return view($this->view);
		}

	}
}
