<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\backend\UserService;
use Redis;

class LoginController extends Controller
{
	public function __construct(UserService $userService)
    {							
		parent::__construct();
		
		$this->userService = $userService;
    }
	
    public function getUser(Request $request){
		
		$queryData = $request->only('name','password');
		
		$user = (array)$this->userService->getUser($queryData);
		
		return $user;
	}
	
}
