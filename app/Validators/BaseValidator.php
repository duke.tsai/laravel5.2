<?php

namespace App\Validators;

use Illuminate\Http\Request;
use Validator;

class BaseValidator
{
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
	
	protected function validate($rules, $messages = array())
    {
		return Validator::make($this->request->all(), $rules, $messages);
    }
}
