<?php

namespace App\Validators\backend;

use Illuminate\Http\Request;
use App\Validators\BaseValidator;


class LoginValidator extends BaseValidator
{
    public function home()
    {	
        return $this->validate([
				'name' => 'required',
				'password' => 'required'
			],
			[
				'name.required' => '帳號為必填欄位',
				'password.required' => '密碼為必填欄位',
			]
		);
    }
}
