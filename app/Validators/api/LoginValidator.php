<?php

namespace App\Validators\api;

use Illuminate\Http\Request;
use App\Validators\BaseValidator;


class LoginValidator extends BaseValidator
{
    public function getUser()
    {	
        return $this->validate(
			[
				'name' => 'required',
				'password' => 'required'
			]
		);
    }
}
