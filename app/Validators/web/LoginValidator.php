<?php

namespace App\Validators\web;

use Illuminate\Http\Request;
use App\Validators\BaseValidator;


class LoginValidator extends BaseValidator
{
    public function login()
    {	
        return $this->validate([
            'name' => 'required',
            'password' => 'required'
        ]);
    }
}
