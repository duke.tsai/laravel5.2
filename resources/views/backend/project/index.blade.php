@extends('backend.layout.app')
	
@section('content')
<form name="myForm" id="myForm" method="post" action="{{ secure_url('project') }}">
{!! csrf_field() !!}
      <table>
		  <tr>
            <td class="Title">Prom Code</td>
            <td>
                <input name="promo_code" type="text" id="promo_code" value="{{ Session::get('promo_code') }}"/>
            </td>
          </tr>
		  <tr>
            <td class="Title">專案名稱</td>
            <td>
                <select id="project_name" name="project_name" >
					<option value="0">請選擇</option>
					<?php foreach($dataList as $data){?>
						<option value="<?php echo $data->project_name; ?>" <?php echo (Session::get('project_name')==$data->project_name)?"selected":"" ?> ><?php echo $data->project_name; ?></option>
					<?php } ?>
				</select>
            </td>
          </tr>
		  <tr>
            <td class="Title">前台顯示</td>
            <td>
                <select id="visible" name="visible">
					<option value="0">請選擇</option>
					<?php for($index=1;$index<=2;$index++){ ?>
						<option value="<?php echo $index; ?>" <?php echo (Session::get('visible')==$index)?"selected":"" ?>><?php echo ($index==1)?"ON":"OFF" ?></option>
					<?php } ?>
				</select>
            </td>
			<td>
				<button type="submit" >查詢</button><input type="button" onclick="clearData()" value="清除">
			</td>
          </tr>
      </table>
</form>
<hr>
<br>    
<div>
<script>
function PopupCenter(url, title, w, h) {
    
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}
function del() {

	$.ajax({
		url: window.location.protocol+"//"+window.location.hostname+window.location.pathname +"/delete",
		data: {
		},
		dataType: 'text',
		success: function(data) {
			alert(data);
			if(data == "刪除成功"){
				document.getElementById("myForm").submit();
			}
		},
		type: 'GET',
		async: false
	});
}
function clickAll(){

	if($("input[name='project_id[]']").prop("checked")){
		$("input[name='project_id[]']").prop("checked", false);
	}else{
		$("input[name='project_id[]']").prop("checked", true);
	}
	
	var checkboxs = document.getElementsByName('project_id[]');
	var idstr = "";
    for(var i=0;i<checkboxs.length;i++){
		idstr += checkboxs[i].id.replace("chk_", "")+",";
	}

	$.ajax({
		url: window.location,
		data: {
		  add_id:idstr.substr(0,idstr.length-1),check:checkboxs[0].checked
		},
		dataType: 'text',
		success: function(data) {
		},
		type: 'GET',
		async: false
	});
}
</script>
<form name="inputForm" method="post" action="{{ secure_url('project/delete') }}">
{!! csrf_field() !!}
	<div style="float:right;padding-right:5%;" >
		<input type="button" onclick="PopupCenter(window.location.protocol+'//'+window.location.hostname+window.location.pathname +'/insert','insertPage',800,600)" value="新增"><input type="button" onclick="del()" value="刪除">
	</div>
    <table border="0" cellpadding="5" cellspacing="1" class="n">
      <tr>                                                
           <th class="TitleTD" width="5%"><a href="javascript:clickAll();" id="clickAll">全選</a></th>
           <th class="TitleTD" width="20%"><a href="<?php echo Session::get('url')."?col=promo_code&sort=".Session::get('sort') ?>">Promo Code</a></th>
           <th class="TitleTD" width="15%"><a href="<?php echo Session::get('url')."?col=project_name&sort=".Session::get('sort') ?>">專案名稱</a></th>
           <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=month&sort=".Session::get('sort') ?>">約期</a></th>
           <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=product&sort=".Session::get('sort') ?>">產品</a></th>
           <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=rate&sort=".Session::get('sort') ?>">速率</a></th>
           <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=pay_type&sort=".Session::get('sort') ?>">繳費方式</a></th>
           <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=price&sort=".Session::get('sort') ?>">繳費金額</a></th>
           <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=plus_price&sort=".Session::get('sort') ?>">加價購</a></th>
		   <th class="TitleTD" width="20%"><a href="<?php echo Session::get('url')."?col=present&sort=".Session::get('sort') ?>">贈品</a></th>
		   <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=penalty&sort=".Session::get('sort') ?>">違約金</a></th>
		   <th class="TitleTD" width="5%"><a href="<?php echo Session::get('url')."?col=visible&sort=".Session::get('sort') ?>">前台顯示</a></th>
      </tr>
	  <?php foreach($dataList as $data){?>
	  <tr class="ListItem" align="center">
		<td><input type="checkbox" id="chk_<?php echo $data->id;?>" name="project_id[]" value="<?php echo $data->id;?>" onClick="idAry(this.id)" <?php echo ((!empty(Session::get('project_ary'))&&in_array($data->id,Session::get('project_ary'))?"checked":"")); ?>></td>
		<td><?php echo $data->promo_code;?></td>
		<td><?php echo $data->project_name;?></td>
		<td><?php echo $data->month;?></td>
		<td><?php echo $data->product;?></td>
		<td><?php echo $data->rate;?></td>
		<td><?php echo $data->pay_type;?></td>
		<td><?php echo $data->price;?></td>
		<td><?php echo $data->plus_price;?></td>
		<td><?php echo $data->present;?></td>
		<td><?php echo $data->penalty;?></td>
		<td>
			<select id="visible_<?php echo $data->id; ?>" onchange="changeVisible(this.id)">
			<?php for($index=1;$index<=2;$index++){ ?>
				<option value="<?php echo $index; ?>" <?php echo ($data->visible==$index)?"selected":"" ?>><?php echo ($index==1)?"ON":"OFF" ?></option>
			<?php } ?>
			</select>
		</td>
	  </tr>
       <?php }?>
    </table>
</form>    
</div>
<div style="display: none;"> 
    <img id="calImg" src="js/datepick/calendar.gif" alt="日期" class="calImg" align="middle"> 
</div>

<script type="text/javascript" src="js/Project.js"></script>
@endsection



