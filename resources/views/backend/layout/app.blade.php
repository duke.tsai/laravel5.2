<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="application/json; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Clear Backend Project</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="backend/js/Tree.js"></script>
	<script type="text/javascript" src="backend/js/List.js"></script>
	<script src="backend/js/jquery/jquery-1.6.4.min.js" type="text/javascript"></script>
	<script src="backend/js/jquery/jquery.metadata.js" type="text/javascript"></script>
	<script src="backend/js/jquery/jquery.validate.min.js" type="text/javascript"></script>
	<script src="backend/js/jquery/jquery.validate.customer.js" type="text/javascript"></script>
	<script src="backend/js/jquery/messages_tw.js" type="text/javascript"></script>
	<script type="text/javascript" src="backend/js/jquery-ui-1.8.10.custom/backend/js/jquery-ui-1.8.10.custom.min.js"></script>
	<!--FancyBox S-->
	<script src="backend/js/jquery.fancybox-1.3.4/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
	<script src="backend/js/jquery.fancybox-1.3.4/jquery.mousewheel-3.0.4.pack.js" type="text/javascript"></script>
	<script type="text/javascript" src="backend/js/datepick/jquery.datepick.min.js"></script>
	<link href="backend/js/datepick/jquery.datepick.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="backend/js/datepick/jquery.datepick.js"></script>
	<script language="javascript">
		$(document).ready(function() {
			$('.dateCal').datepick('option', { showAnim: 'fadeIn', showSpeed: 'fast' });
			$('.dateCalImg').datepick('option', { showAnim: 'fadeIn', showSpeed: 'fast' });
			$('.BirthdayCal').datepick('option', { showAnim: 'fadeIn', showSpeed: 'fast' });
			$('.BirthdayCalImg').datepick('option', { showAnim: 'fadeIn', showSpeed: 'fast' });
			//預設日期(defaultDate), y:年, w:週, d:天, ex.明年:1y, 上周:-1w, 明天:1d
			//$('.dateCal').datepick({ selectDefaultDate: true, defaultDate: '1d', showTrigger: '#calImg' });
			$('.dateCal').datepick();
			$('.dateCalImg').datepick({
				defaultDate: 'd', 
				//selectDefaultDate: true,
				showTrigger: '#calImg',
				dateFormat: 'yyyy-mm-dd',
				yearRange: '1950:2050',
			});
			$('.BirthdayCal').datepick({ defaultDate: '-20y' });
			$('.BirthdayCalImg').datepick({ defaultDate: '-20y', showTrigger: '#calImg' });
		});
	</script>
	<link href="backend/css/AdminMenu.css" type="text/css" rel="stylesheet" />
	<link href="backend/css/Page.css" type="text/css" rel="stylesheet" />
	<link href="backend/css/Basic.css" type="text/css" rel="stylesheet" />
	
	
	<style>
	/*增加手指符號*/
	.hand{
		cursor:pointer;
	}

	/*增加隱藏*/
	.no_show{
		display:none;
	}
	</style>
</head>
<body>
<table width="100%" align="center">
   <tr>      
		<td id="ctl00_tdLeftMenu" valign="top" style="width: 200px;" class="leftmenu">
			<div id="menuContent">
				<div id="topBorder">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td id="topLeftBorder"><a href="{{ secure_url('logout') }}">登出</a></td>
							<td id="topMiddleBorder"></td>
							<td id="topRightBorder"></td>
						</tr>
					</table>
				</div>
				<br>
				Hi, {{ Session::get('user')->account }}, {{ Session::get('user')->name }};
				<br>
				<br>
				<?php foreach(Session::get('menuList') as $parent) : ?>
					<div class="folder">				
							<a href="<?php echo $parent->url; ?>" target="_self"><div id="item<?php echo $parent->id; ?>" class="function"><?php echo $parent->name; ?></div></a>
					</div>
				<?php endforeach; ?>
			</div>
		</td>
		<td id="ctl00_tdMainArea" valign="top">
			<div id="msg"></div>
			<div class="title">{{ Session::get('title') }}</div>
			<div style="padding:5px;"></div>
		
			@yield ('content')
			
			@include ('backend.layout.page')
		</td>
	</tr>
</table>
</body>
</html>
