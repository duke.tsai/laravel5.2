<?php if(isset($dataList) && !empty($dataList) && $dataList->lastPage() > 1){ ?>
	<div>
	
	<?php if(empty($dataList->previousPageUrl())){ ?>
		<span class="inactivePrev">上一頁</span>
	<?php }else{ ?>
		<a class="prev" href="{{$dataList->previousPageUrl()}}">上一頁</a>
	<?php } ?>
	
	<?php foreach($page as $index => $p){ ?>
		<?php if($index == 0 && $p >= 2){ ?>
			<a class="paginate" href="<?php echo Session::get('url')."?page=1" ?>">1...</a>
		<?php }else if($index == 8 && $p < $dataList->lastPage()){ ?>
			<a class="paginate" href="<?php echo Session::get('url')."?page=".$dataList->lastPage() ?>"><?php echo "...".$dataList->lastPage(); ?></a>
		<?php }else{ ?>
			<?php if($p <= $dataList->lastPage()){ ?>
				<?php if($p == $dataList->currentPage()){ ?> 
					<a class="current" ><?php echo $p ?></a>
				<?php }else{ ?>
					<a class="paginate" href="<?php echo Session::get('url')."?page=".$p; ?>"><?php echo $p; ?></a>
				<?php } ?>
			<?php } ?>
		<?php } ?>
	<?php } ?>
	
	<?php if(empty($dataList->nextPageUrl())){ ?>
		<span class="inactiveNext">下一頁</span>
	<?php }else{ ?>
		<a class="next" href="{{$dataList->nextPageUrl()}}">下一頁</a>
	<?php } ?>
	
	</div>
<?php } ?>
