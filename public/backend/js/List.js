$(document).ready(function () {
    LoadList();
})

function LoadList() {
    $(".ListItem:even").addClass("ListItem_even");
    $(".ListItem:odd").addClass("ListItem_odd");

    $(".ListItem").mouseover(function (e) {
        $(this).children("*").css("background-color", "#CCFFCC")
    })

    $(".ListItem").mouseout(function (e) {
        $(this).children("*").css("background-color", "")
    })

    $(".ListRow").mouseover(function (e) {
        $(this).children("*").css("background-color", "#CCFFCC")
    })

    $(".ListRow").mouseout(function (e) {
        $(this).children("*").css("background-color", "")
    })
}