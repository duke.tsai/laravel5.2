function checkDisplay(pId){
    if($("#"+pId).next(".content").css("display")=="none"){
        show(pId)
    }else{
        hide(pId)
    }
}
function show(pId){
    $("#"+pId).addClass('btOpen')
    $("#"+pId).removeClass('btClose')
    $("#"+pId).next(".content").slideDown("fast")
}
function hide(pId){
    $("#"+pId).addClass('btClose')
    $("#"+pId).removeClass('btOpen')
    $("#"+pId).next(".content").slideUp("fast")
}
function defaultOpen(pId){
    if(pId != ''){
	    $("#folderIcon"+pId).removeClass('btClose');
	    $("#folderIcon"+pId).addClass('btOpen');
	    $("#"+pId+">.content").show();
    }
}