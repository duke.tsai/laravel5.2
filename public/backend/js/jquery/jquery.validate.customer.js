
$.validator.addMethod("pwdcheck",
     function( value, element){
         //var mask = /^[0-9]{6}$/;
         //(?=.*[a-zA-Z])(?=.*[@#$%^&+=])
         var mask = /^(?=.*[a-z]{2,14})(?=.*\d{2,14}).{8,16}$/
         if(!mask.exec(value)){
            return false;    
         }else{
            return true;    
         }
     }, '8~16字元並含最少2個英文字或2個數字'
);

$.validator.addMethod("pwdconfirmchk",
     function( value, element){
         //var mask = /^[0-9]{6}$/;
         //(?=.*[a-zA-Z])(?=.*[@#$%^&+=])
         if(value != $('#pwd1').attr('value')){
            return false;    
         }else{
            return true;    
         }
     }, '請確認第二次輸入的密碼是正確的'
);

$.validator.addMethod("populace_required",
     function( value, element){
         var id = $(element).attr('id').split('_')[1];
         return ( $('#populace_code_'+id).is(':checked') ) ?
             ( value != '') : true;
     }, '請選擇身份別'
);